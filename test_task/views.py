from django.shortcuts import render, redirect


def main(request):
    return render(request,
                  'test_task/base.html')
