from django.db import models


class Menu(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Меню'
        verbose_name_plural = 'Меню'


class MenuItem(models.Model):
    menu = models.ForeignKey(Menu, verbose_name='Меню', )
    name = models.CharField(verbose_name='Название пункта', max_length=50)
    slug = models.CharField(max_length=200)
    url = models.CharField(max_length=100, help_text='Ссылка, например /about/ или http://foo.com/')
    named = models.BooleanField(default=False, verbose_name='Использовать named url')
    parent = models.ForeignKey('self', blank=True, null=True, related_name='children',
                               verbose_name='Родительский пункт')

    def save(self, *args, **kwargs):
        if self.parent:
            self.slug = '%s%s/' % (self.parent.slug, self.name)
        else:
            self.slug = '%s/' % self.name
        super().save()

    def __str__(self):
        return self.slug

    class Meta:
        verbose_name = 'Пункт меню'
        verbose_name_plural = 'Пункты меню'
