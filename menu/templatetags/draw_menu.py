from django import template
from django.core.exceptions import EmptyResultSet

from ..models import MenuItem

register = template.Library()


@register.inclusion_tag(filename='menu/menu.html')
def draw_menu(menu_name):
    menu_items = MenuItem.objects.filter(menu__name=menu_name)

    if not menu_items:
        raise EmptyResultSet

    context = {'items': menu_items}
    return context
