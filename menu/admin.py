from django.contrib import admin
from .models import MenuItem, Menu


class MenuItemAdmin(admin.ModelAdmin):
    fields = ['menu', 'name', 'url', 'named', 'parent']

    class Meta:
        model = MenuItem


admin.site.register(Menu)
admin.site.register(MenuItem, MenuItemAdmin)
